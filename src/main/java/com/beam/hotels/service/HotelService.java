package com.beam.hotels.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beam.hotels.config.Common;
import com.beam.hotels.dao.HotelRepository;
import com.beam.hotels.dao.ProviderRepository;
import com.beam.hotels.dto.BaseHotelResponse;
import com.beam.hotels.dto.HotelRequestParams;
import com.beam.hotels.dto.HotelResponseFactory;
import com.beam.hotels.model.Hotel;
import com.beam.hotels.model.Provider;

/**
 * HotelService is a bridge  between the controllers(client) and the repository(data access layer)
 * @author ihab
 *
 */
@Service
//TODO : enhance this service using query builder instead building all possible quries   
public class HotelService {
	
	@Autowired
	private HotelRepository hotelRepository;
	
	@Autowired
	private ProviderRepository providerRepository;
	
	@Autowired
	private HotelResponseFactory hotelResponseFactory;
	
	
	public Iterable<Hotel> findAll() {
		return hotelRepository.findAll();
	}
	
	public List<BaseHotelResponse> searchAvailableHotel(HotelRequestParams params) {
		Iterable<Hotel> searchResults = null;
		//handle possible params combinations by routing to different repository query
		if(params.getCity()!=null && params.getNumberOfAdults()!=null && params.getFromDate()!=null&&params.getToDate()!=null ) {
			searchResults = hotelRepository.searchHotels(params.getCity(),params.getNumberOfAdults(),params.getFromDate() , params.getToDate());	
		}else if(params.getCity()!=null && params.getNumberOfAdults()!=null ) {
			searchResults = hotelRepository.findByCityAndAdultsCount(params.getCity() , params.getNumberOfAdults());
		}else if( params.getFromDate()!=null&&params.getToDate()!=null) {
			searchResults = hotelRepository.findByFromAndTo(params.getFromDate(), params.getToDate());
		}else if(params.getNumberOfAdults()!=null) {
			searchResults = hotelRepository.findByAdultsCount((params.getNumberOfAdults()));
		}else if(params.getCity()!=null) {
			searchResults = hotelRepository.findByCity(params.getCity());
		}
		
		return toDTOList(searchResults , Common.ResponseFormat.DEFAULT.name() , params);
	}
	 
	
	public List<BaseHotelResponse>searchProviderAvailableHotel(HotelRequestParams params) {

		//handle possible params combinations by routing to different repository query
		
		if(params.getAdultsCount() == null && params.getNumberOfAdults()!= null) {
			params.setAdultsCount(params.getNumberOfAdults());
		}
		
		Iterable<Hotel> searchResults = null;

		if(params.getCity()!=null && params.getAdultsCount()!=null && params.getProvider()!=null  ) {
			if(params.getFromDate()!=null&&params.getToDate()!=null) {
				searchResults = hotelRepository.searchProviderHotels(params.getCity(),params.getAdultsCount(),params.getFromDate() , params.getToDate() ,params.getProvider());
			}else if(params.getFrom()!=null&&params.getTo()!=null) {
				searchResults = hotelRepository.searchProviderHotelsByInstantDates(params.getCity(),params.getAdultsCount(),params.getFrom() , params.getTo() ,params.getProvider());
			}else {
				searchResults = hotelRepository.findByCityAndAdultsCountAndProvider(params.getCity(),params.getAdultsCount(),params.getProvider());
			}
		}else if(params.getFromDate()!=null&&params.getToDate()!=null && params.getProvider()!=null) {
			searchResults = hotelRepository.findByFromAndToAndProvider(params.getFromDate(), params.getToDate(),params.getProvider());
		}else if(params.getFrom()!=null&&params.getTo()!=null&&params.getProvider()!=null) {
			searchResults = hotelRepository.findByFromAndToInstantDatesAndProvider(params.getFrom(), params.getTo(),params.getProvider());
		}else if(params.getCity()!=null && params.getProvider()!=null) {
			searchResults = hotelRepository.findByCityAndProvider(params.getCity() , params.getProvider());
		}else if(params.getProvider()!=null) {
			searchResults = hotelRepository.findByProvider(params.getProvider());
		}
		
				
		Provider hotelProvider = providerRepository.findByName(params.getProvider());
		
		return toDTOList(searchResults,hotelProvider.getOutputFormat() , params);
	}
	
	/**
	 * service util class used to wrap the db entity created by repository into the right DTO to be returned to the caller. 
	 * @param searchHotelsResult db entity
	 * @param format the output json layout
	 * @param params request params
	 * @return list of DTOs
	 */
	private  List<BaseHotelResponse> toDTOList(Iterable<Hotel> searchHotelsResult,String format, HotelRequestParams params) {
		List<BaseHotelResponse> responseList = new ArrayList<>();
		searchHotelsResult.forEach(hotel->{
			BaseHotelResponse hotelResponse = hotelResponseFactory.getHotelResponse(Common.ResponseFormat.valueOf(format));
			hotelResponse.setHotel(hotel);
			hotelResponse.setRequestParams(params);
			responseList.add(hotelResponse);
		});
		return responseList;
	}


}
