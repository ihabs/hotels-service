package com.beam.hotels.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.beam.hotels.dto.validation.ResponseStatus;
/**
 * generic exception handling controller maps all the errors to common json format 
 * @author ihab
 *
 */
@ControllerAdvice
public class ExceptionController  {
  
	@ExceptionHandler(BindException.class)
	public final ResponseEntity<List<ResponseStatus>> handleBindExceptions(Exception e, WebRequest request) {
		List<ResponseStatus> errors = new ArrayList<>();
		ResponseStatus responseStatus = new ResponseStatus("error", "unable to map request parameter(s) read the api doc: http://localhost:8080/ ");
		errors.add(responseStatus);
	  return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(NoHandlerFoundException.class)
	public final ResponseEntity<List<ResponseStatus>> handleNotFoundExceptions(Exception e, WebRequest request) {
		List<ResponseStatus> errors = new ArrayList<>();
		ResponseStatus responseStatus = new ResponseStatus("error", "resource not found read the api doc: http://localhost:8080/ ");
		errors.add(responseStatus);
	  return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<List<ResponseStatus>> handleAllExceptions(Exception e, WebRequest request) {
		List<ResponseStatus> errors = new ArrayList<>();
		ResponseStatus responseStatus = new ResponseStatus("error", e.getMessage()!=null?e.getMessage():" api error read the api doc: http://localhost:8080/");
		errors.add(responseStatus);
	  return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	

			
	
	
}
