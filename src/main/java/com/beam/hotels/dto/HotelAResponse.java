package com.beam.hotels.dto;

import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Component
@Scope("prototype")
@JsonInclude(Include.NON_NULL)
public class HotelAResponse extends BaseHotelResponse {

	public String getHotelName() {
		return hotel.getName();
	}

	public String getRate() {
		String rateStars = "";
		for (int i = 0; i < hotel.getRate(); i++) {
			rateStars += "*";
		}
		return rateStars;
	}
	
	public Float getPrice() {
		return hotel.getFare();
	}
	
	public Integer getDiscount() {
		return hotel.getDiscount();
	}
	
	public Set<String> getAmenities() {
		return formatAmenities();
	}

}
