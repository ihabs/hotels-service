package com.beam.hotels.dto.validation;

import com.beam.hotels.dto.BaseHotelResponse;

public class ResponseStatus extends BaseHotelResponse {
	
	
	private String status;
	
	private String message;
	
	public ResponseStatus(String status,String message) {
		this.status = status;
		this.message = message;
	}

	public ResponseStatus() {
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
	

}
