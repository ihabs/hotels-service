
create table provider (id int not null auto_increment, name varchar(255), output_format varchar(1), primary key (id));

create table hotel (
id int not null auto_increment,
name varchar(255), 
provider_id int,
rate int,
fare float,
discount int, 
city varchar(3), 
primary key (id),
foreign key (provider_id) references provider(id)
);

create table amenitiy (id int not null auto_increment, name varchar(255), primary key (id));



create table room (
id int not null auto_increment,
hotel_id int not null,
adults_count int,
fare float,
foreign key (hotel_id) references hotel(id),
primary key (id));

create table hotel_amenitiy (
  hotel_id int not null,
  amenitiy_id int not null,
  primary key (hotel_id,amenitiy_id),
	foreign key (hotel_id) references hotel(id),
	foreign key (amenitiy_id) references amenitiy(id)
);

create table room_amenitiy (
  room_id int not null,
  amenitiy_id int not null,
  primary key (room_id,amenitiy_id),
	foreign key (room_id) references room(id),
	foreign key (amenitiy_id) references amenitiy(id)
);

create table room_booking (
id int not null auto_increment,
room_id int not null,
from_date datetime,
to_date datetime,
primary key (id),
foreign key (room_id) references room(id)
);

