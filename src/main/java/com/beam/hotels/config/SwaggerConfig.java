package com.beam.hotels.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * open API (Swagger) config
 * @author ihab
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	private static final Contact CONTACT = 
			new Contact("Ihab Shehadeh", "https://www.linkedin.com/in/ihab-shehadeh-31b85916/", "ihab.shh@gmailcom");

	private static final ApiInfo API_INFO = new ApiInfoBuilder().contact(CONTACT).build();
	
	private static final Set<String> CONTENT_TYPES = new HashSet<String>(Arrays.asList("application/json"));

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(API_INFO)
				.produces(CONTENT_TYPES)
				.consumes(CONTENT_TYPES) 
				.tags(new Tag("AvailableHotelService", "Available hotels service is a simple API hotels search service."),
					  new Tag("ProviderService", "Provider hotels service is a hotels search service for a specific provider."));
	
	}
	

}
