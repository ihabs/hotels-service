package com.beam.hotels.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.beam.hotels.config.Common;
import com.beam.hotels.dto.validation.ResponseStatus;

public class HotelRequestParams {

	private String city;
	@DateTimeFormat(pattern = Common.ISO_LOCAL_DATE_FORMAT)
	private Date fromDate;
	@DateTimeFormat(pattern = Common.ISO_LOCAL_DATE_FORMAT)
	private Date toDate;
	private String provider;
	private Integer adultsCount;
	private Integer numberOfAdults;
	@DateTimeFormat(pattern = Common.ISO_INSTANT_DATE_FORMAT)
	private Date from;
	@DateTimeFormat(pattern = Common.ISO_INSTANT_DATE_FORMAT)
	private Date to;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Integer getAdultsCount() {
		return adultsCount;
	}

	public void setAdultsCount(Integer adultsCount) {
		this.adultsCount = adultsCount;
	}

	public Integer getNumberOfAdults() {
		return numberOfAdults;
	}

	public void setNumberOfAdults(Integer numberOfAdults) {
		this.numberOfAdults = numberOfAdults;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public List<BaseHotelResponse> getValidationErrors() {
		ResponseStatus responseStatus = new ResponseStatus();
		// validation rules
		List<BaseHotelResponse> errors = new ArrayList<>();
		Boolean fromToDate = (fromDate != null && toDate != null);
		Boolean fromTo = (from != null && to != null);

		Boolean one = (from != null || to != null || fromDate != null || toDate != null);

		Boolean valid = !one || (fromToDate || fromTo);

		responseStatus.setStatus(valid ? "success" : "error");

		responseStatus.setMessage(valid ? "success" : "missing parameter one of the two date combinations is required : fromDate,toDate or from,to ");

		if (!valid) {
			errors.add(responseStatus);
		}
		return errors;
	}

}
