package com.beam.hotels.dto;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.beam.hotels.model.Amenitiy;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Component
@Scope("prototype")
@JsonInclude(Include.NON_NULL)
public class HotelBResponse extends BaseHotelResponse {

	public String getHotel() {
		return hotel.getName();
	}

	public Integer getHotelRate() {
		return hotel.getRate();
	}

	public Float getHotelFare() {
		long numberOfNights = 0l;
		if(params.getFrom() != null && params.getTo() != null) {
			numberOfNights = TimeUnit.DAYS.convert(params.getTo().getTime()-params.getFrom().getTime(), TimeUnit.MILLISECONDS);
		} else if (params.getFromDate() != null && params.getToDate() != null)  {
			numberOfNights = TimeUnit.DAYS.convert(params.getToDate().getTime()-params.getFromDate().getTime(), TimeUnit.MILLISECONDS);
		}
		return (float)(Math.round(hotel.getFare()*numberOfNights  * 100.0) / 100.0) ;
	}

	public String getRoomAmenities() {
		 String amenitiesCSV = "";
		 Iterator<Amenitiy> amenitiesIterator = hotel.getAmenities().iterator();
		 while (amenitiesIterator.hasNext()) {
		 Amenitiy amenity = amenitiesIterator.next();
		 amenitiesCSV+=amenity.getName()+ (amenitiesIterator.hasNext()?",":"");
		 }
		 return amenitiesCSV;
	}
	
	
	
	
}
