package com.beam.hotels.dto;

import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Component
@Scope("prototype")
@JsonInclude(Include.NON_NULL)
public class HotelResponse extends BaseHotelResponse {

	public String getProvider() {
		return hotel.getProvider().getName();
	}

	public String getHotelName() {
		return hotel.getName();
	}

	public Float getFare() {
		return hotel.getFare();
	}

	public Set<String> getAmenities() {
		return formatAmenities();
	}

}
