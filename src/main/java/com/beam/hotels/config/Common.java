package com.beam.hotels.config;

/**
 * common config class holds App constants 
 * @author ihab
 *
 */
public abstract class Common {
	
	public static final String API_PREFIX = "/api/v1"; 
	
	public static final String ISO_LOCAL_DATE_FORMAT = "yyyy-MM-dd";
	
	public static final  String ISO_INSTANT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	
	//Identifiers of 3 Different output json layouts 
	public enum ResponseFormat {
		A , B , DEFAULT
	}


}
