package com.beam.hotels.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table
public class Room {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Integer id;

	@Column
	private Integer adultsCount;

	@Column
	private Float fare;

	@ManyToOne
    @JoinColumn(name="hotel_id")
	@JsonBackReference
	private Hotel hotel;
	
	@OneToMany(mappedBy="room")
	@JsonManagedReference
	private Set<RoomBooking> roomBookings;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "room_amenitiy", joinColumns = { @JoinColumn(name = "room_id") }, inverseJoinColumns = { @JoinColumn(name = "amenitiy_id") })
	private Set<Amenitiy> amenitiys;


	public Set<RoomBooking> getRoomBookings() {
		return roomBookings;
	}

	public void setRoomBookings(Set<RoomBooking> roomBookings) {
		this.roomBookings = roomBookings;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Set<Amenitiy> getAmenitiys() {
		return amenitiys;
	}

	public void setAmenitiys(Set<Amenitiy> amenitiys) {
		this.amenitiys = amenitiys;
	}

	public Integer getAdultsCount() {
		return adultsCount;
	}

	public void setAdultsCount(Integer adultsCount) {
		this.adultsCount = adultsCount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Float getFare() {
		return fare;
	}

	public void setFare(Float fare) {
		this.fare = fare;
	}

}
