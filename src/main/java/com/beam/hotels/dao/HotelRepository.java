package com.beam.hotels.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.beam.hotels.config.SQLQueries;
import com.beam.hotels.dto.HotelRequestParams;
import com.beam.hotels.model.Hotel;
/**
 * provides the Hotel entity Crud operations.
 * @author ihab
 *
 */
public interface HotelRepository extends CrudRepository<Hotel, Long> {
	
	Iterable<Hotel> findAll();
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY )
	Iterable<Hotel> searchHotels(@Param("city") String city, @Param("adultsCount") Integer adultsCount, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate) ;
	
	Iterable<Hotel> findByCity(@Param("city") String city);
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_CITY_AND_NUM_OF_ADULTS )
	Iterable<Hotel> findByCityAndAdultsCount(@Param("city") String city, @Param("adultsCount") Integer adultsCount);
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_CITY_AND_PROVIDER )
	Iterable<Hotel> findByCityAndProvider(@Param("city") String city,@Param("provider") String provider);
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_NUM_OF_ADULTS )
	Iterable<Hotel> findByAdultsCount(@Param("adultsCount") Integer adultsCount);

	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_FROM_TO_INSTANT_DATES_QUERY )
	Iterable<Hotel> findByFromAndToInstantDates(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_FROM_TO )
	Iterable<Hotel> findByFromAndTo(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
	

	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_FROM_TO_INSTANT_DATES_QUERY_AND_PROVIDER )
	Iterable<Hotel> findByFromAndToInstantDatesAndProvider(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate,@Param("provider") String provider);
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_FROM_TO_AND_PROVIDER )
	Iterable<Hotel> findByFromAndToAndProvider(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate,@Param("provider") String provider);
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_PROVIDER_HOTELS_QUERY)
	Iterable<Hotel> searchProviderHotels(@Param("city") String city, @Param("adultsCount") Integer adultsCount, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate,@Param("provider") String provider) ;
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_PROVIDER_HOTELS_BY_INSTANT_DATES_QUERY)
	Iterable<Hotel> searchProviderHotelsByInstantDates(@Param("city") String city, @Param("adultsCount") Integer adultsCount, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate,@Param("provider") String provider) ;


	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_CITY_AND_NUM_OF_ADULTS_AND_PROVIDER )
	Iterable<Hotel> findByCityAndAdultsCountAndProvider(@Param("city") String city, @Param("adultsCount") Integer adultsCount,@Param("provider") String provider);
	
	@Query(nativeQuery=true, value = SQLQueries.SEARCH_HOTELS_QUERY_BY_PROVIDER )
	Iterable<Hotel> findByProvider(@Param("provider") String provider);


}
