

-- insert 2 initail providers
INSERT INTO provider(name,output_format) VALUES ('CrazyHotels' , 'A');
INSERT INTO provider(name,output_format) VALUES ('BestHotels' , 'B');

-- insert amenities
INSERT INTO amenitiy(name) VALUES ('swimming pool');
INSERT INTO amenitiy(name) VALUES ('mini fridge');
INSERT INTO amenitiy(name) VALUES ('gym');
INSERT INTO amenitiy(name) VALUES ('rooftop');

-- insert hotels 
INSERT INTO hotel(name , provider_id , rate , fare, city ) VALUES ('First World Hotel' , 1  , 2 , 630, 'AUH');
INSERT INTO hotel(name , provider_id , rate, fare, city ,discount) VALUES ('M Hotel' , 1  , 5 , 280 , 'AUH' , 20);
INSERT INTO hotel(name , provider_id, rate , fare , city ,discount) VALUES ('Caesars Palace' , 2  , 5 , 757.589  , 'DXB' ,50);

-- insert hotel amenitiy
INSERT INTO hotel_amenitiy(hotel_id, amenitiy_id) VALUES (1 ,1 );
INSERT INTO hotel_amenitiy(hotel_id, amenitiy_id) VALUES (2 ,1 );
INSERT INTO hotel_amenitiy(hotel_id, amenitiy_id) VALUES (3 ,1 );
INSERT INTO hotel_amenitiy(hotel_id, amenitiy_id) VALUES (3 ,3 );
INSERT INTO hotel_amenitiy(hotel_id, amenitiy_id) VALUES (3 ,4 );

-- insert hotels rooms
INSERT INTO room(hotel_id, adults_count , fare ) VALUES (1 ,5 ,750.5 );
INSERT INTO room(hotel_id, adults_count , fare ) VALUES (2 ,4 ,750.5 );
INSERT INTO room(hotel_id, adults_count , fare ) VALUES (2 ,5 ,450.5 );
INSERT INTO room(hotel_id, adults_count , fare ) VALUES (3 ,2 ,450 );

-- insert room amenitiy
INSERT INTO room_amenitiy(room_id, amenitiy_id) VALUES (1 ,2 );
INSERT INTO room_amenitiy(room_id, amenitiy_id) VALUES (2 ,2 );
INSERT INTO room_amenitiy(room_id, amenitiy_id) VALUES (3 ,2 );

-- insert room booking
INSERT INTO room_booking(room_id, from_date, to_date) VALUES (1 ,PARSEDATETIME ('10-10-2018 11:34:24','dd-MM-yyyy hh:mm:ss'),PARSEDATETIME ('20-10-2018 11:34:24','dd-MM-yyyy hh:mm:ss') );
INSERT INTO room_booking(room_id, from_date, to_date) VALUES (1 ,PARSEDATETIME ('25-10-2018 11:34:24','dd-MM-yyyy hh:mm:ss'),PARSEDATETIME ('28-10-2018 11:34:24','dd-MM-yyyy hh:mm:ss') );
