package com.beam.hotels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring APP entry point.
 * @author ihab
 *
 */
@SpringBootApplication
public class AvailableHotelsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvailableHotelsServiceApplication.class, args);
	}
}
