package com.beam.hotels;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AvailableHotelServiceTests {

	@Autowired
	private MockMvc mockMvc;
	
	private static final String BASE_URL= "http://localhost:8080/api/v1";

	@Test
	public void testValidHotelSearch() throws Exception {

		this.mockMvc.perform(get(BASE_URL+"/AvailableHotel?city=AUH&numberOfAdults=5&fromDate=2018-11-23&toDate=2018-11-29").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"));

	}
	
	@Test
	public void testHotelSearchInvalidParam() throws Exception {

		this.mockMvc.perform(get(BASE_URL+"/Hotel?city=AUH&numberOfAdults=5&fromDate=2018-11-23&toDate=2018-11-29").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().is5xxServerError())
				.andExpect(content().contentType("application/json;charset=UTF-8"));

	}
	
	@Test
	public void testHotelSearchInvalidType() throws Exception {

		this.mockMvc.perform(get(BASE_URL+"/Hotel?city=AUH&numberOfAdults=5&fromDate=2018-11-23&to=2018-11-29").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().is4xxClientError())
				.andExpect(content().contentType("application/json;charset=UTF-8"));

	}

}
