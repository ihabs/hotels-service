-Project description: 

 Hotels service project is a simple rest API hotels search service.
 
-Tech stack: 


Language: Java 

Frameworks/APIs: Spring framework, JPA 

Web container: Apache Tomcat 8.5.34 

Unit testing: spring-test , JUINT 

Database: H2 (volatile in-memory embedded database used for the coding challenge purpose only see : src/main/resources/

schema.sql and data.sql) 

 
-Runtime environment requirements: 

Building and packaging : Apache Maven 3.5.2  

Runtime : Java 1.8 SE Runtime Environment 


-Running the project : 

1.Build and packaging : mvn package  

2.Running project fat/uber JAR : java -jar target/available-hotels-service-0.0.1.jar 


-Open API : 

I integrated swagger open API library to document the service and for easier testing :  

http://localhost:8080/ 

examples :

http://localhost:8080/api/v1/AvailableHotel?city=AUH&numberOfAdults=5&fromDate=2018-11-23&toDate=2018-11-29

http://localhost:8080/api/v1/BestHotels?city=DXB&adultsCount=2&fromDate=2018-11-23&toDate=2018-11-29

http://localhost:8080/api/v1/CrazyHotels?city=AUH&adultsCount=5&from=2018-10-10T9:15:30.653Z&to=2018-10-10T11:15:30.653Z


-Database ERD diagram

/docs/DB-ERD-diagram.pdf


-Error format :

all the APIs and the generic errors requests will be mapped to standard json error with the right HTTP status code. 


-Adding new provider : can be done easily by exposing the provider repository save entity, however this API should 

deployed  and secured separately.
    

    