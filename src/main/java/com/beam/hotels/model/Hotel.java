package com.beam.hotels.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table
public class Hotel {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String name;

	@ManyToOne
	@JoinColumn(name = "provider_id")
	private Provider provider;

	@Column
	private String city;

	@Column
	private Integer rate;

	@Column
	private Float fare;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "hotel_amenitiy", joinColumns = { @JoinColumn(name = "hotel_id") }, inverseJoinColumns = { @JoinColumn(name = "amenitiy_id") })
	private Set<Amenitiy> amenities;

	@OneToMany(mappedBy = "hotel")
	@JsonManagedReference
	private Set<Room> rooms;
	
	@Column
	private Integer discount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public Float getFare() {
		return fare;
	}

	public void setFare(Float fare) {
		this.fare = fare;
	}

	public Set<Amenitiy> getAmenities() {
		return amenities;
	}

	public void setAmenities(Set<Amenitiy> amenities) {
		this.amenities = amenities;
	}

	public Set<Room> getRooms() {
		return rooms;
	}

	public void setRooms(Set<Room> rooms) {
		this.rooms = rooms;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}



}
