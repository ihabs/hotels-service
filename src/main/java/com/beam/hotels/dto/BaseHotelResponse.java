package com.beam.hotels.dto;

import java.util.HashSet;
import java.util.Set;

import com.beam.hotels.model.Amenitiy;
import com.beam.hotels.model.Hotel;

public abstract class BaseHotelResponse {

	protected Hotel hotel;
	
	protected HotelRequestParams params;
	
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public void setRequestParams(HotelRequestParams params) {
		this.params = params;
	}
	
	public Set<String> formatAmenities() {
		Set<String> amenitiesArray = new HashSet<>();
		for (Amenitiy amenitiy : hotel.getAmenities()) {
			amenitiesArray.add(amenitiy.getName());
		}
		return amenitiesArray;
	}
	
	
	
	
}
