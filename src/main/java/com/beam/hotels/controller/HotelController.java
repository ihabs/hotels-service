package com.beam.hotels.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.beam.hotels.config.Common;
import com.beam.hotels.dto.BaseHotelResponse;
import com.beam.hotels.dto.HotelRequestParams;
import com.beam.hotels.service.HotelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * HotelController registers the AvailableHotelService API to be handled using the hotel service.
 * @author ihab
 *
 */
@RestController
@RequestMapping(Common.API_PREFIX)
@Api(tags = { "AvailableHotelService" })
public class HotelController {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	protected HotelService hotelService;
	
	@GetMapping("/AvailableHotel")
	@ApiOperation(value = "AvailableHotel service searching available Hotels by city, date range and number of adults")
	public ResponseEntity<List<BaseHotelResponse>> availableHotel(@ModelAttribute HotelRequestParams params) {
		if(params.getValidationErrors().size() > 0 ) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(params.getValidationErrors());
		}
		return  ResponseEntity.status(HttpStatus.OK).body(hotelService.searchAvailableHotel(params)) ;
	}
	
	

}
