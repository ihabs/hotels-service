package com.beam.hotels.config;

/**
 * common config class holds all the used sql queries  
 * @author ihab
 *
 */
public abstract class SQLQueries {

	//search hotels queries
	public static final String SEARCH_HOTELS_QUERY = "select h.* from hotel h join room r where h.id=r.hotel_id and h.id Not in "
			+ "(select DISTINCT h.id from hotel h,room r join ROOM_BOOKING b on r.id =b.room_id and h.id = r.hotel_id "  
			+ " where (DATEADD(day, 1, :fromDate) <=b.from_date and DATEADD(day, 1, :toDate) >= b.to_date)"
			+ " or (DATEADD(day, 1, :fromDate)  BETWEEN b.from_date and b.to_date "
			+ "or DATEADD(day, 1, :toDate)   BETWEEN b.from_date and b.to_date))"
			+ " and r.adults_count = :adultsCount and h.city = :city order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_FROM_TO = "select DISTINCT h.* from hotel h join room r where h.id=r.hotel_id and h.id Not in "
			+ "(select DISTINCT h.id from hotel h,room r join ROOM_BOOKING b on r.id =b.room_id and h.id = r.hotel_id "  
			+ " where (DATEADD(day, 1, :fromDate) <=b.from_date and DATEADD(day, 1, :toDate) >= b.to_date)"
			+ " or (DATEADD(day, 1, :fromDate)  BETWEEN b.from_date and b.to_date "
			+ "or DATEADD(day, 1, :toDate)   BETWEEN b.from_date and b.to_date))"
			+ "  order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_NUM_OF_ADULTS ="select h.* from hotel h join room r where "
			+ "h.id=r.hotel_id and r.adults_count = :adultsCount order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_CITY_AND_NUM_OF_ADULTS = "select h.* from hotel h join room r "
			+ "where h.id=r.hotel_id and r.adults_count = :adultsCount and h.city = :city order by rate DESC;";
	

	//search providers hotels queries
	public static final String SEARCH_PROVIDER_HOTELS_QUERY = "select h.* from hotel h join room r join PROVIDER p on h.provider_id=p.id"
			+ " where h.provider_id=p.id and h.id=r.hotel_id and h.id Not in "
			+ "(select DISTINCT h.id from hotel h,room r join ROOM_BOOKING b on r.id =b.room_id and h.id = r.hotel_id "  
			+ " where (DATEADD(day, 1, :fromDate) <=b.from_date and DATEADD(day, 1, :toDate) >= b.to_date)"
			+ " or (DATEADD(day, 1, :fromDate)  BETWEEN b.from_date and b.to_date "
			+ "or DATEADD(day, 1, :toDate)   BETWEEN b.from_date and b.to_date))"
			+ " and r.adults_count = :adultsCount and h.city = :city and p.name = :provider order by rate DESC;";
	
	public static final String SEARCH_PROVIDER_HOTELS_BY_INSTANT_DATES_QUERY = "select h.* from hotel h join room r join PROVIDER p on h.provider_id=p.id"
			+ " where h.provider_id=p.id and h.id=r.hotel_id and h.id Not in "
			+ "(select DISTINCT h.id from hotel h,room r join ROOM_BOOKING b on r.id =b.room_id and h.id = r.hotel_id "  
			+ " where (:fromDate <=b.from_date and :toDate >= b.to_date)"
			+ " or (:fromDate BETWEEN b.from_date and b.to_date "
			+ "or :toDate BETWEEN b.from_date and b.to_date))"
			+ " and r.adults_count = :adultsCount and h.city = :city and p.name = :provider order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_CITY_AND_NUM_OF_ADULTS_AND_PROVIDER = "select h.* from hotel h join room r join PROVIDER p on h.provider_id=p.id "
			+ "where h.id=r.hotel_id and r.adults_count = :adultsCount and h.city = :city and p.name = :provider order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_PROVIDER = "select  DISTINCT h.* from hotel h join room r join PROVIDER p on h.provider_id=p.id "
			+ "where h.id=r.hotel_id and p.name = :provider order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_CITY_AND_PROVIDER =  "select DISTINCT h.* from hotel h join room r join PROVIDER p on h.provider_id=p.id "
			+ "where h.id=r.hotel_id and p.name = :provider and h.city = :city order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_FROM_TO_INSTANT_DATES_QUERY =  "select DISTINCT h.* from hotel h join room r where h.id=r.hotel_id and h.id Not in "
			+ "(select DISTINCT h.id from hotel h,room r join ROOM_BOOKING b on r.id =b.room_id and h.id = r.hotel_id "  
			+ " where :fromDate <=b.from_date and :toDate >= b.to_date)"
			+ " or :fromDate  BETWEEN b.from_date and b.to_date "
			+ "or :toDate  BETWEEN b.from_date and b.to_date))"
			+ "  order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_FROM_TO_INSTANT_DATES_QUERY_AND_PROVIDER =  "select DISTINCT h.* from hotel h join room r join PROVIDER p on h.provider_id=p.id where h.id=r.hotel_id and h.id Not in "
			+ "(select DISTINCT h.id from hotel h,room r join ROOM_BOOKING b on r.id =b.room_id and h.id = r.hotel_id "  
			+ " where :fromDate <=b.from_date and :toDate >= b.to_date"
			+ " or :fromDate  BETWEEN b.from_date and b.to_date "
			+ "or :toDate  BETWEEN b.from_date and b.to_date)"
			+ " and p.name = :provider order by rate DESC;";
	
	public static final String SEARCH_HOTELS_QUERY_BY_FROM_TO_AND_PROVIDER = "select DISTINCT h.* from hotel h join room r join PROVIDER p on h.provider_id=p.id where h.id=r.hotel_id and h.id Not in "
			+ "(select DISTINCT h.id from hotel h,room r join ROOM_BOOKING b on r.id =b.room_id and h.id = r.hotel_id "  
			+ " where (DATEADD(day, 1, :fromDate) <=b.from_date and DATEADD(day, 1, :toDate) >= b.to_date)"
			+ " or (DATEADD(day, 1, :fromDate)  BETWEEN b.from_date and b.to_date "
			+ "or DATEADD(day, 1, :toDate)   BETWEEN b.from_date and b.to_date))"
			+ " and p.name = :provider  order by rate DESC;";
	
}
