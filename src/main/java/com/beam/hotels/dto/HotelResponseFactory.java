package com.beam.hotels.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.beam.hotels.config.Common;
/**
 * HotelResponseFactory creates the right dto object based on the provider format config.
 * @author ihab
 *
 */
@Component
public class HotelResponseFactory {

	@Autowired
	private ApplicationContext ctx;

	public BaseHotelResponse getHotelResponse(Common.ResponseFormat responseFormat) {
		BaseHotelResponse hotelResponse = null;
		switch (responseFormat) {
		case A:
			hotelResponse = ctx.getBean(HotelAResponse.class);
			break;
		case B:
			hotelResponse = ctx.getBean(HotelBResponse.class);
			break;
		default:
			hotelResponse = ctx.getBean(HotelResponse.class);
			break;
		}
		return hotelResponse;
	}

}
