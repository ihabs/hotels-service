package com.beam.hotels.dao;

import org.springframework.data.repository.CrudRepository;

import com.beam.hotels.model.Provider;

/**
 * provides the Provider entity Crud operations.
 * @author ihab
 *
 */
public interface ProviderRepository extends CrudRepository<Provider, Long> {
	
	Provider findByName(String name);
	

}
