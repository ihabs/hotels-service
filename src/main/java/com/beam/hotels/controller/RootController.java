package com.beam.hotels.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import springfox.documentation.annotations.ApiIgnore;

/**
 * redirects the root to swagger ui page.
 * @author ihab
 *
 */
@Controller 
@ApiIgnore
public class RootController {
	
	@GetMapping("/")
	public String rootRedirect() {
		return "redirect:/swagger-ui.html";
	}
	

}
